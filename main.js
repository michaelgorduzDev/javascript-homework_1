// Task 1

// Як можна оголосити змінну у Javascript?
// We can use "let" for mutable variables, and "const" for immutable variables.
// Also we can use "var", but it's not recommended anymore

let admin;
let myName = "Michael";
admin = myName;

console.log(admin);

// Task 2

// У чому різниця між функцією prompt та функцією confirm?
// Prompt allows us to let the user to enter some information, and store it in variable if needed.
// Confirm only have 2 options - yes or no

let days = 8;
let oneDay = 60*60*24;
const totalSeconds = oneDay*days;

console.log("8 days contains "+totalSeconds+" seconds");

// Task 3

// Що таке неявне перетворення типів? Наведіть один приклад.
// That means that JavaScript can automatically convert different types of data.
// Example:
//  let a = 3;
//  let b = "25";
//  let c = a = b;
//  console.log(c);

const question = document.getElementById("btn");
function ask() {
  const promption = prompt("What is your name?");
  console.log("Your name is "+promption);
}